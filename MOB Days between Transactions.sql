-- MOB Analysis

with #newmembers as 
(select a.csn, membersince, min(transaction_date) as FirstTransaction from link.view_facttrans_plus a join link.link_client_master b on a.csn=b.csn
where yearid in ('2018','2019') and upper(a.club_code)='LNK' and upper(pool_id)='P01' and upper(cancel_ind)='N' and pointid='I' and transcount=1
and membersince>'2018-01-01' and membersince<'2019-02-01'
group by a.csn, membersince having FirstTransaction>=membersince),

#transactions as
(select a.csn, membersince, transaction_date, row_number() over (partition by a.csn order by transaction_date asc) as transaction from #new a join link.view_facttrans_plus b on a.csn=b.csn
where datediff(day, membersince, transaction_date)<=365 and upper(club_code)='LNK' and upper(pool_id)='P01' and upper(cancel_ind)='N' and pointid='I' and transcount=1),

#transactiongaps as
(select a.csn, b.transaction as n, datediff(day, a.transaction_date, b.transaction_date) as gap from #transactions a left join #transactions b on a.csn=b.csn and a.transaction+1=b.transaction
where n<11
union
select csn, 1 as n, datediff(day, membersince, transaction_date) as gap from #transactions where transaction=1)

select n, avg(gap), count(distinct csn)
from #transactiongaps
group by n
order by n asc;
