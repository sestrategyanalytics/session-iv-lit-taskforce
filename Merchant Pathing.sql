drop table if exists #path;
select csn, 
case when corporation_id in ('FP', 101770000000) then 'NTUC FairPrice'
when corporation_id in (select corporation_id from lac.ntucclub_merchantlisting) and branch_no in (select branch_no from lac.ntucclub_merchantlisting) then 'NTUC Club'
when corporation_id=104940000000 then 'Caltex'
when corporation_id=100360000000 then 'Poh Heng'
when corporation_id=104970000000 then 'Courts'
when corporation_id=100270000000 then 'Lim & Tan'
when corporation_id=100460000000 then 'ZTP'
when corporation_id=107200000000 then 'Select'
when corporation_id=105730000000 then 'Metro'
when corporation_id=105700000000 then 'Car Club'
when corporation_id=106020000000 then 'Texas Chicken'
when corporation_id=105110000000 then 'The Face Shop'
else 'Others'
end as merchant, 
min(transaction_date) as firsttransaction, row_number() over(partition by csn order by firsttransaction asc) as path
into #path
from link.view_facttrans_plus a
join link.link_cls_corporation b on a.corporation_id=b.corporate_id
where pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>='2018-01-01' and est_gross_transaction_amt>0 and transcount=1
group by csn, merchant;

-- mob
drop table if exists #merchantpath;
with #joindate as
(select a.csn, case when membersince<=min(transaction_date) then membersince else min(transaction_date) end as joindate
from link.link_client_master a
left join link.view_facttrans_plus b on a.csn=b.csn
where pool_id='P01' and b.club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0
group by a.csn, membersince)

select a.csn,  joindate, case when corporation_id in ('FP', 101770000000) then 'NTUC FairPrice'
when corporation_id in (select corporation_id from lac.ntucclub_merchantlisting) and branch_no in (select branch_no from lac.ntucclub_merchantlisting) then 'NTUC Club'
when corporation_id=104940000000 then 'Caltex'
when corporation_id=100360000000 then 'Poh Heng'
when corporation_id=104970000000 then 'Courts'
when corporation_id=100270000000 then 'Lim & Tan'
when corporation_id=100460000000 then 'ZTP'
when corporation_id=107200000000 then 'Select'
when corporation_id=105730000000 then 'Metro'
when corporation_id=105700000000 then 'Car Club'
when corporation_id=106020000000 then 'Texas Chicken'
when corporation_id=105110000000 then 'The Face Shop'
else 'Others'
end as merchant, min(transaction_date) as firsttransaction, row_number() over(partition by a.csn order by firsttransaction asc) as path
into #merchantpath
from #joindate a
left join link.view_facttrans_plus b on a.csn=b.csn
join link.link_cls_corporation c on b.corporation_id=c.corporate_id
where pool_id='P01' and b.club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0
group by joindate, a.csn, merchant
order by joindate desc, a.csn, path;

drop table if exists lac.csn_merchantpath;
select csn
max(case when path=1 then merchant end) as first_merchant,
max(case when path=2 then merchant end) as second_merchant,
max(case when path=3 then merchant end) as third_merchant,
max(case when path=4 then merchant end) as fourth_merchant,
max(case when path=5 then merchant end) as fifth_merchant
into lac.csn_merchantpath
from #merchantpath
group by csn;

select * from lac.csn_merchantpath;
