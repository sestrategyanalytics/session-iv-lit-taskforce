drop table if exists #monthlyspend;
select * into #monthlyspend from
(select '<2016' as vintage, a.csn, sum(est_gross_transaction_amt)/36.0 as monthlyspend,
sum(case when corporation_id='OCBC' then est_gross_transaction_amt else 0 end)/36.0 as ocbcspend,
sum(case when corporation_id!='OCBC' then est_gross_transaction_amt else 0 end)/36.0 as otherspend,
sum(transcount)/36.0 as monthlyfreq, datediff(month, membersince, getdate()) as mob
from link.link_client_master a
left join link.view_facttrans_plus b on a.csn=b.csn
where yearid in ('2016','2017','2018') and upper(b.club_code)='LNK' and upper(pool_id)='P01' and upper(cancel_ind)='N' and pointid='I'
and (membersince<'2016-01-01' or membersince is NULL)
group by vintage, a.csn, mob
union
select '2016' as vintage, a.csn, sum(est_gross_transaction_amt)/(datediff(month, membersince, getdate())) as monthlyspend, 
sum(case when corporation_id='OCBC' then est_gross_transaction_amt else 0 end)/(datediff(month, membersince, getdate())) as ocbcspend,
sum(case when corporation_id!='OCBC' then est_gross_transaction_amt else 0 end)/(datediff(month, membersince, getdate())) as otherspend,
sum(transcount)/36.0 as monthlyfreq, datediff(month, membersince, getdate()) as mob
from link.link_client_master a
left join link.view_facttrans_plus b on a.csn=b.csn
where yearid in ('2016','2017','2018') and upper(b.club_code)='LNK' and upper(pool_id)='P01' and upper(cancel_ind)='N' and pointid='I'
and (membersince>='2016-01-01' and membersince<'2017-01-01')
group by vintage, a.csn, membersince, mob
union
select '2017' as vintage, a.csn, sum(est_gross_transaction_amt)/(datediff(month, membersince, getdate())) as monthlyspend, 
sum(case when corporation_id='OCBC' then est_gross_transaction_amt else 0 end)/(datediff(month, membersince, getdate())) as ocbcspend,
sum(case when corporation_id!='OCBC' then est_gross_transaction_amt else 0 end)/(datediff(month, membersince, getdate())) as otherspend,
sum(transcount)/36.0 as monthlyfreq, datediff(month, membersince, getdate()) as mob
from link.link_client_master a
left join link.view_facttrans_plus b on a.csn=b.csn
where yearid in ('2016','2017','2018') and upper(b.club_code)='LNK' and upper(pool_id)='P01' and upper(cancel_ind)='N' and pointid='I'
and (membersince>='2017-01-01' and membersince<'2018-01-01')
group by vintage, a.csn, membersince, mob
union
select '2018' as vintage, a.csn, sum(est_gross_transaction_amt)/(datediff(month, membersince, getdate())) as monthlyspend, 
sum(case when corporation_id='OCBC' then est_gross_transaction_amt else 0 end)/(datediff(month, membersince, getdate())) as ocbcspend,
sum(case when corporation_id!='OCBC' then est_gross_transaction_amt else 0 end)/(datediff(month, membersince, getdate())) as otherspend,
sum(transcount)/36.0 as monthlyfreq, datediff(month, membersince, getdate()) as mob
from link.link_client_master a
left join link.view_facttrans_plus b on a.csn=b.csn
where yearid in ('2016','2017','2018') and upper(b.club_code)='LNK' and upper(pool_id)='P01' and upper(cancel_ind)='N' and pointid='I'
and (membersince>='2018-01-01' and membersince<'2018-10-01')
group by vintage, a.csn, membersince, mob);

-- split ocbc and non-ocbc
drop table if exists #CLTV;
select a.csn, a.vintage, mob, monthlyspend, ocbcspend, otherspend, monthlyfreq, active, membership_type, acquisition_channel, 
gender_code, marital_status, dob, datediff(year, dob, getdate()) as age, postal_code, race_code, segment_desc,
case when comms_by_sms_flag ='Y' AND valid_sms ='Y' then 'Y' else 'N' end as SMS, case when (comms_by_email_flag = 'Y' and valid_email = 'Y' and hard_bounce = 'N') then 'Y' else 'N' end as EDM,
case when a.vintage='<2016' then 0.08 else 0.18 end as decay,
ocbcspend*12*(2-decay) as expectedocbcspend,
otherspend*12*(2-decay) as expectedotherspend,
expectedocbcspend*0.017+expectedotherspend*0.0016 as CLTV
into #CLTV
from #monthlyspend a
join link.link_client_master b on a.csn=b.csn
left join lac.client_segmentation_cluster d on a.csn=d.csn
left join lac.client_segmentation_cluster_desc e on d.cluster=e.segment;

-- $32.38
select count (distinct csn), sum(expectedocbcspend), sum(expectedotherspend), avg(CLTV) from #CLTV;

-- combine ocbc and non-ocbc spend
drop table if exists #CLTV;
select a.csn, a.vintage, mob, monthlyspend, ocbcspend, otherspend, monthlyfreq, active, membership_type, acquisition_channel, 
gender_code, marital_status, dob, datediff(year, dob, getdate()) as age, postal_code, race_code, segment_desc,
case when comms_by_sms_flag ='Y' AND valid_sms ='Y' then 'Y' else 'N' end as SMS, case when (comms_by_email_flag = 'Y' and valid_email = 'Y' and hard_bounce = 'N') then 'Y' else 'N' end as EDM,
case when a.vintage='<2016' then 0.08 else 0.18 end as decay,
monthlyspend*12*(2-decay) as expectedspend,
expectedspend*0.002633 as CLTV
into #CLTV
from #monthlyspend a
join link.link_client_master b on a.csn=b.csn
left join lac.client_segmentation_cluster d on a.csn=d.csn
left join lac.client_segmentation_cluster_desc e on d.cluster=e.segment;

select top 1000 * from #CLTV;

-- overall CLTV: 12.94
select 'Overall' as Grouping, avg(cltv) as CLTV, count(distinct csn) from #CLTV
union
select case when vintage='<2016' then 'Existing before 2016' else 'New since 2016' end as Grouping, avg(cltv) as CLTV, count(distinct csn) from #CLTV
group by Grouping
union
select segment_desc as Grouping, avg(cltv) as CLTV, count(distinct csn) from #CLTV
group by Grouping;

-- Campus Invasion
drop table if exists #campus;
create table #campus(csn nvarchar(50));
copy #campus from 's3://link-lac-data/Manfred/20190201_campus_invasion.csv' 
access_key_id 'AKIAJ3BMMACHUPUSVIDQ'
secret_access_key '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'
REGION  'ap-southeast-1'
delimiter ',' 
IGNOREHEADER 1 
CSV QUOTE '\"';

-- campus: 2.90
select avg(cltv) from #CLTVperpetuity
where csn in (select csn from #campus);

-- overall: 2.33
select avg(cltv) from #CLTVperpetuity
where age > 17 and age < 26;

-- Perpetuity
drop table if exists #CLTVperpetuity;
select a.csn, a.vintage, mob, monthlyspend, ocbcspend, otherspend, monthlyfreq, active, membership_type, acquisition_channel, 
gender_code, marital_status, dob, datediff(year, dob, getdate()) as age, postal_code, race_code, segment_desc,
case when comms_by_sms_flag ='Y' AND valid_sms ='Y' then 'Y' else 'N' end as SMS, case when (comms_by_email_flag = 'Y' and valid_email = 'Y' and hard_bounce = 'N') then 'Y' else 'N' end as EDM,
case when a.vintage='<2016' then 0.08 else 0.18 end as decay,
monthlyspend*12/decay as expectedspend,
expectedspend*0.002633 as CLTV
into #CLTVperpetuity
from #monthlyspend a
join link.link_client_master b on a.csn=b.csn
left join lac.client_segmentation_cluster d on a.csn=d.csn
left join lac.client_segmentation_cluster_desc e on d.cluster=e.segment;

-- overall CLTV: 13.92
select 'Overall' as Grouping, avg(cltv) as CLTV, count(distinct csn) from #CLTVperpetuity
union
select case when vintage='<2016' then 'Existing before 2016' else 'New since 2016' end as Grouping, avg(cltv) as CLTV, count(distinct csn) from #CLTVperpetuity
group by Grouping
union
select segment_desc as Grouping, avg(cltv) as CLTV, count(distinct csn) from #CLTVperpetuity
group by Grouping;




-- find decay for members since 2016
drop table if exists #newin2016transactions;
with #members as
(select csn, membersince
from link.link_client_master
where extract(year from membersince)=2016)

select a.csn, membersince, min(datediff(day, membersince, transaction_date)) as FirstTransaction, max(datediff(day, membersince, transaction_date)) as LastTransaction
into #newin2016transactions
from #members b left join link.view_facttrans_plus a on b.csn = a.csn
join link.link_cls_corporation c on a.corporation_id=c.corporate_id
where upper(club_code)='LNK' and upper(pool_id)='P01' and upper(cancel_ind)='N' and pointid='I' and est_gross_transaction_amt>0 and yearid>=2016
group by a.csn, membersince
having FirstTransaction>=0 and LastTransaction>=0;

-- 104,991
select count(csn) from #newin2016transactions;

-- 84,630
select count(csn) from #newin2016transactions where LastTransaction>365;

-- 65,851
select count(csn) from #newin2016transactions where LastTransaction>730;

-- find decay for rest
drop table if exists #otherstransactions;
with #members as
(select a.csn, min(transaction_date) as FirstTransaction
from link.link_client_master a
join link.view_facttrans_plus b on a.csn=b.csn
where upper(b.club_code)='LNK' and upper(pool_id)='P01' and upper(cancel_ind)='N' and pointid='I' and est_gross_transaction_amt>0 and yearid=2016
and extract(year from membersince)!='2016'
group by a.csn)

select a.csn, FirstTransaction, max(datediff(day, FirstTransaction, transaction_date)) as LastTransaction
into #otherstransactions
from #members b left join link.view_facttrans_plus a on b.csn = a.csn
join link.link_cls_corporation c on a.corporation_id=c.corporate_id
where upper(club_code)='LNK' and upper(pool_id)='P01' and upper(cancel_ind)='N' and pointid='I' and est_gross_transaction_amt>0 and yearid>=2016
group by a.csn, FirstTransaction
having LastTransaction>=0;

-- 1,062,065
select count(csn) from #otherstransactions;

-- 974,917
select count(csn) from #otherstransactions where LastTransaction>365;

-- 892,793
select count(csn) from #otherstransactions where LastTransaction>730;
