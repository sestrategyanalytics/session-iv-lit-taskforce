drop table if exists lac.csn_merchantpath;

drop table if exists #csn_joindate;
select a.csn, case when membersince>min(transaction_date) then null else membersince end as joindate
into #csn_joindate
from link.view_facttrans_plus a
join link.link_client_master b on a.csn=b.csn
group by a.csn, membersince;

drop table if exists #path;
select a.csn, joindate,
case when corporation_id in ('FP', 101770000000) then 'NTUC FairPrice'
when corporation_id in (select corporation_id from lac.ntucclub_merchantlisting) and branch_no in (select branch_no from lac.ntucclub_merchantlisting) then 'NTUC Club'
when corporation_id=104940000000 then 'Caltex'
when corporation_id=100360000000 then 'Poh Heng'
when corporation_id=104970000000 then 'Courts'
when corporation_id=100270000000 then 'Lim & Tan'
when corporation_id=100460000000 then 'ZTP'
when corporation_id=107200000000 then 'Select'
when corporation_id=105730000000 then 'Metro'
when corporation_id=105700000000 then 'Car Club'
when corporation_id=106020000000 then 'Texas Chicken'
when corporation_id=105110000000 then 'The Face Shop'
when upper(corporate_name_english_1) like '%FOODFARE%' then 'NTUC Foodfare'
when corporation_id=105790000000 then 'EZ-Link'
when corporation_id=105820000000 then 'Mothercare'
when corporation_id=103780000000 then 'Skechers'
when corporation_id=104050000000 then 'Zalora'
when corporation_id=103710000000 then 'Ma Kuang'
when corporation_id in (102200000000, 101780000000) then 'Cheers'
when corporation_id=105920000000 then 'Superpets'
else 'Others'
end as merchant, 
transaction_date, row_number() over(partition by a.csn order by transaction_date asc) as path
into #path
from link.view_facttrans_plus a
join link.link_cls_corporation b on a.corporation_id=b.corporate_id
join #csn_joindate c on a.csn=c.csn
where pool_id='P01' and a.club_code='LNK' and cancel_ind='N' and pointid='I' and est_gross_transaction_amt>0 and transcount=1 and transaction_date>=joindate and transaction_date<=dateadd(month, 6, joindate)
group by a.csn, joindate, corporation_id, corporate_name_english_1, branch_no, transaction_date;

select csn, joindate, max(path),
max(case when path=1 then merchant end) as first_merchant,
max(case when path=2 then merchant end) as second_merchant,
max(case when path=3 then merchant end) as third_merchant,
max(case when path=4 then merchant end) as fourth_merchant,
max(case when path=5 then merchant end) as fifth_merchant
into lac.csn_merchantpath
from #path
where joindate>='2017-01-01' and joindate<=dateadd(month, -3, getdate()) and joindate is not null and path<=5
group by csn, joindate;

grant select on lac.csn_merchantpath to group lacusers;
