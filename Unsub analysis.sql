drop table if exists lac.edm_unsub_cardtype;
drop table if exists #unsubs;
drop table if exists #unsubs_cardtype;

select csn, max(start_date) as unsub_date into #unsubs from link.link_edm_sms_sub_hist
where sub_status=0 and comm_type='EDM'
and csn in
(select csn from link.link_edm_sms_sub_hist
where sub_status=1 and comm_type='EDM')
group by csn, sub_status 
having max(start_date)>='2019-01-01';

-- 24,585 (3,047 preissue)
select a.csn, max(case when card_no like '810500%' then 'preissue' else 'other' end) as cardtype, unsub_date from #unsubs a join link.link_cls_card b on a.csn=b.csn
group by a.csn, unsub_date;

select csn, max(start_date) as unsub_date into #uncontactable from link.link_edm_sms_sub_hist
where sub_status=0 and comm_type='EDM'
and csn not in
(select csn from link.link_edm_sms_sub_hist
where sub_status=1 and comm_type='EDM')
group by csn, sub_status 
having max(start_date)>='2019-01-01';

-- 9,096 (1,135 preissue)
select a.csn, max(case when card_no like '810500%' then 'preissue' else 'other' end) as cardtype, unsub_date from #uncontactable a join link.link_cls_card b on a.csn=b.csn
group by a.csn, unsub_date;

-- contactable
select csn, max(start_date) as unsub_date into #contactable from link.link_edm_sms_sub_hist
where sub_status=1 and comm_type='EDM'
and csn not in
(select csn from link.link_edm_sms_sub_hist
where sub_status=0 and comm_type='EDM')
group by csn, sub_status 
having max(start_date)>='2019-01-01';

-- 98,808 (31,067 perso)
select a.csn, max(case when card_no like '810501%' then 'perso' else 'other' end) as cardtype, unsub_date from #contactable a join link.link_cls_card b on a.csn=b.csn
group by a.csn, unsub_date;


select a.csn, sum(est_gross_transaction_amt) as sales, sum(transcount) as transactions, membersince, active, gender_code, marital_status, dob from #unsubs a 
join link.link_client_master b on a.csn=b.csn
join link.view_facttrans_plus c on a.csn=c.csn
group by a.csn, membersince, active, gender_code, marital_status, dob;

select a.csn, max(case when card_no like '810500%' then 'preissue' else 'other' end) as cardtype, unsub_date from #unsubs2018 a join link.link_cls_card b on a.csn=b.csn
group by a.csn, unsub_date;



select csn, max(start_date) as unsub_date into #unsubs2018 from link.link_edm_sms_sub_hist
where sub_status=0 and comm_type='EDM'
and csn in
(select csn from link.link_edm_sms_sub_hist
where sub_status=1 and comm_type='EDM')
group by csn, sub_status 
having max(start_date)>='2018-01-01' and max(start_date)<'2019-01-01'


select csn, max(start_date) as unsub_date into #contactable from link.link_edm_sms_sub_hist
where sub_status=1 and comm_type='EDM'
and csn not in
(select csn from link.link_edm_sms_sub_hist
where sub_status=0 and comm_type='EDM')
group by csn, sub_status 
having max(start_date)>='2019-01-01';

-- 9,096 (1,135 preissue)
select a.csn, max(case when card_no like '810500%' then 'preissue' else 'other' end) as cardtype, unsub_date from #contactable a join link.link_cls_card b on a.csn=b.csn
group by a.csn, unsub_date;



select a.csn, max(case when card_no like '810501%' then 'perso' else 'other' end) as cardtype, unsub_date 
into #unsubs_cardtype
from #unsubs a join link.link_cls_card b on a.csn=b.csn
group by a.csn, unsub_date;

select unsub_date, sum(case when cardtype='perso' then 1 else 0 end) as perso,
sum(case when cardtype='other' then 1 else 0 end) as other
into lac.edm_unsub_cardtype
from #unsubs_cardtype
group by unsub_date
order by unsub_date asc;

grant select on lac.edm_unsub_cardtype to group lacusers;

drop table if exists #subs;
select csn, sub_status, max(start_date) as sub_date,
row_number() over(partition by csn order by sub_date desc) as sequence
into #subs
from link.link_edm_sms_sub_hist
where comm_type='EDM'
group by csn, sub_status;

select top 10000 * from #subs;

-- 2,304,552
select count(distinct csn) from #subs
where sub_status=1 and sequence=1;

select count(distinct csn) from link.link_client_master where comms_by_email_flag = 'Y' AND valid_email = 'Y' AND hard_bounce ='N';
