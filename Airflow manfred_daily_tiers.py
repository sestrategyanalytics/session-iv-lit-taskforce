import psycopg2

try: 
    conn = psycopg2.connect(dbname='redshift01', host='nonprod-analytics-redshit01.cvdlsqkayfsg.ap-southeast-1.redshift.amazonaws.com',\
    port='5439', user='lac', password='Link1234') 
except: 
    print("Unable to connect to the database.")
    
cur=conn.cursor()
cur.execute('''
drop table if exists lac.csn_tiers;

select csn, sum(est_gross_transaction_amt) as spend, sum(transcount) as transactions, NTILE(10) over(order by spend desc) as tier
into #tiers
from link.view_facttrans_plus
where pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointid='I' and transaction_date>=dateadd(month, -12, getdate()) and est_gross_transaction_amt>0
group by csn
order by spend desc;

select tier, a.csn, spend, case when transactions>0 then transactions else 1 end as transactioncount, spend/transactioncount as basketsize, active, membership_type, gender_code, datediff(year, dob, getdate()) as age, postal_code, segment_desc, explaination as segment_exp
into lac.csn_tiers
from #tiers a
join link.link_client_master b on a.csn=b.csn
left join lac.client_segmentation_cluster d on a.csn=d.csn
left join lac.client_segmentation_cluster_desc e on d.cluster=e.segment;

grant select on lac.csn_tiers to group lacusers;
''')

conn.commit()
conn.close()
cur.close()