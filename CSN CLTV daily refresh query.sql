drop table if exists lac.csn_cltv;

with #monthlyspend as
(select '<2016' as vintage, a.csn, case when membersince>min(transaction_date) then null else membersince end as membersince, sum(est_gross_transaction_amt)/24 as monthlyspend
from link.link_client_master a
left join link.view_facttrans_plus b on a.csn=b.csn
where yearid>=2016 and upper(b.club_code)='LNK' and upper(pool_id)='P01' and upper(cancel_ind)='N' and pointid='I'
and (membersince<'2016-01-01' or membersince is null)
and transaction_date>=dateadd(year, -2, getdate())
group by vintage, a.csn, membersince
union
select 'new' as vintage, a.csn, case when membersince>min(transaction_date) then null else membersince end as membersince, sum(est_gross_transaction_amt)/(case when datediff(month, membersince, getdate())<24 then datediff(month, membersince, getdate()) else 24 end) as monthlyspend
from link.link_client_master a
left join link.view_facttrans_plus b on a.csn=b.csn
where yearid>=2016 and upper(b.club_code)='LNK' and upper(pool_id)='P01' and upper(cancel_ind)='N' and pointid='I'
and (membersince>='2016-01-01' and membersince<=dateadd(month, -3, getdate()))
and transaction_date>=dateadd(year, -2, getdate())
group by vintage, a.csn, membersince),

#CLTV as
(select csn, vintage, membersince, monthlyspend, case when vintage='<2016' then 0.08 else 0.18 end as decay,
monthlyspend*12*(2-decay) as forecast2yGMV,
forecast2yGMV*0.002633 as forecast2yCLTV,
monthlyspend*12/decay as forecastGMV,
forecastGMV*0.002633 as forecastCLTV
from #monthlyspend)

select a.csn, case when membersince>min(transaction_date) then null else membersince end as membersince, monthlyspend, min(transaction_date) as FirstTransaction, max(transaction_date) as LastTransaction, sum(transcount) as Transactions, count(distinct corporation_id) as Merchants,
sum(est_gross_transaction_amt) as GMVtodate, GMVtodate*0.002633 as CLTVtodate, forecast2yGMV, forecast2yCLTV, forecastGMV, forecastCLTV, GMVtodate+forecastGMV as TotalGMV, CLTVtodate+forecastCLTV as TotalCLTV
into lac.csn_cltv
from link.view_facttrans_plus a right join #CLTV b on a.csn=b.csn
where yearid>=2016 and upper(club_code)='LNK' and upper(pool_id)='P01' and upper(cancel_ind)='N' and pointid='I'
group by a.csn, membersince, monthlyspend, forecast2yGMV, forecast2yCLTV, forecastGMV, forecastCLTV;

grant select on lac.csn_cltv to group lacusers;

select top 1000 * from lac.csn_cltv;
