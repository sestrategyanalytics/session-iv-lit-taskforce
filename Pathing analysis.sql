drop table if exists #transactions;
with #members as
(select csn, min(transaction_date) as FirstTransaction
from link.view_facttrans_plus
where upper(club_code)='LNK' and upper(pool_id)='P01' and upper(cancel_ind)='N' and pointid='I' and est_gross_transaction_amt>0
group by csn
having min(transaction_date) between '2018-01-01' and '2018-01-31')

select a.csn, row_number() over (partition by a.csn order by transaction_date asc) as TransNumber, transaction_date as TransactionDate, FirstTransaction, datediff(day, FirstTransaction, transaction_date) as DaysSinceFirst,
corporate_name_english_1 as Merchant, est_gross_transaction_amt as GMV, point_earned as LinkPoints 
into #transactions
from link.view_facttrans_plus a join #members b on a.csn = b.csn
join link.link_cls_corporation c on a.corporation_id=c.corporate_id
where upper(club_code)='LNK' and upper(pool_id)='P01' and upper(cancel_ind)='N' and pointid='I' and est_gross_transaction_amt>0 and yearid>=2018 and transcount=1;

(select transnumber, merchant, count(csn) from #transactions
where transnumber=1
group by transnumber, merchant
order by count desc
limit 10)
union
(select transnumber, merchant, count(csn) from #transactions
where transnumber=2
group by transnumber, merchant
order by count desc
limit 10)
union
(select transnumber, merchant, count(csn) from #transactions
where transnumber=3
group by transnumber, merchant
order by count desc
limit 10)
union
(select transnumber, merchant, count(csn) from #transactions
where transnumber=4
group by transnumber, merchant
order by count desc
limit 10)
union
(select transnumber, merchant, count(csn) from #transactions
where transnumber=5
group by transnumber, merchant
order by count desc
limit 10)
order by transnumber, count desc;

select transnumber, count(csn)
from #transactions
group by transnumber;
