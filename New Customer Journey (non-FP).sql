-- All transactions made by "all members active between 2017 Jan and 2018 Jun"
drop table if exists #transactions;
with #members as
(select csn, min(transaction_date) as FirstTransaction
from link.view_facttrans_plus
where upper(club_code)='LNK' and upper(pool_id)='P01' and upper(cancel_ind)='N' and pointid='I' and est_gross_transaction_amt>0
and yearid in ('2017','2018')
group by csn
having min(transaction_date) between '2017-01-01' and '2018-06-01')

select a.csn, transaction_date as TransactionDate, FirstTransaction, datediff(day, FirstTransaction, transaction_date) as DaysSinceFirst, 
case 
  when DaysSinceFirst<=30 then '1'
  when DaysSinceFirst>30 and DaysSinceFirst<=60 then '2'
  when DaysSinceFirst>60 and DaysSinceFirst<=90 then '3'
  when DaysSinceFirst>90 and DaysSinceFirst<=120 then '4'
  when DaysSinceFirst>120 and DaysSinceFirst<=150 then '5'
  when DaysSinceFirst>150 and DaysSinceFirst<=180 then '6'
  else 'beyond6' end as Period,
corporate_name_english_1 as Merchant, corporation_id, est_gross_transaction_amt as GMV, transcount as Transactions, point_earned as LinkPoints 
into #transactions
from link.view_facttrans_plus a join #members b on a.csn = b.csn
join link.link_cls_corporation c on a.corporation_id=c.corporate_id
where upper(club_code)='LNK' and upper(pool_id)='P01' and upper(cancel_ind)='N' and pointid='I' and est_gross_transaction_amt>0 and yearid>=2017;

drop table if exists #customerlevel;
select csn, Period, sum(GMV) as GMV, sum(Transactions) as Transactions, sum(LinkPoints) as LinkPoints, count(distinct Merchant) as UniqueMerchants, 'Internal' as Flag
into #customerlevel
from #transactions
where corporation_id in ('OCBC','FP','101770000000','100020000000','101780000000')
group by csn, Period
union
select csn, Period, sum(GMV) as GMV, sum(Transactions) as Transactions, sum(LinkPoints) as LinkPoints, count(distinct Merchant) as UniqueMerchants, 'External' as Flag
from #transactions
where corporation_id not in ('OCBC','FP','101770000000','100020000000','101780000000')
group by csn, Period;

select Flag, count(distinct csn) as Customers, sum(GMV)/Customers as Spend, sum(Transactions/1.0)/Customers as Transactions, avg(UniqueMerchants/1.0) from #customerlevel
where period='1'
group by Flag;

select count(distinct csn) from #customerlevel where period='1' and Flag='External'
and csn not in (select csn from #customerlevel where period='1' and Flag='Internal');

-- 60 days
select count(distinct csn), avg(Transactions/1.0), avg(LinkPoints), avg(UniqueMerchants/1.0)
from #customerlevel
where csn in (select csn from #customerlevel where Period='2')
and Period='1'
union
select count(distinct csn), avg(Transactions/1.0), avg(LinkPoints), avg(UniqueMerchants/1.0)
from #customerlevel
where csn not in (select csn from #customerlevel where Period='2')
and Period='1';

-- 90 days
select count(distinct csn), avg(Transactions/1.0), avg(LinkPoints), avg(UniqueMerchants/1.0)
from #customerlevel
where csn in (select csn from #customerlevel where Period='1')
and csn in (select csn from #customerlevel where Period='3')
and Period='2'
union
select count(distinct csn), avg(Transactions/1.0), avg(LinkPoints), avg(UniqueMerchants/1.0)
from #customerlevel
where csn in (select csn from #customerlevel where Period='1')
and csn not in (select csn from #customerlevel where Period='3')
and Period='2';

select count(distinct csn), avg(Transactions/1.0), avg(LinkPoints), avg(UniqueMerchants/1.0)
from #customerlevel
where csn in (select csn from #customerlevel where Period='3')
and csn not in (select csn from #customerlevel where Period='2')
and Period='1'
union
select count(distinct csn), avg(Transactions/1.0), avg(LinkPoints), avg(UniqueMerchants/1.0)
from #customerlevel
where csn not in (select csn from #customerlevel where Period='3')
and csn not in (select csn from #customerlevel where Period='2')
and Period='1';

-- 120 days
select count(distinct csn), avg(Transactions/1.0), avg(LinkPoints), avg(UniqueMerchants/1.0)
from #customerlevel
where csn in (select csn from #customerlevel where Period='1')
and csn in (select csn from #customerlevel where Period='2')
and csn in (select csn from #customerlevel where Period='4')
and Period='3'
union
select count(distinct csn), avg(Transactions/1.0), avg(LinkPoints), avg(UniqueMerchants/1.0)
from #customerlevel
where csn in (select csn from #customerlevel where Period='1')
and csn in (select csn from #customerlevel where Period='2')
and csn not in (select csn from #customerlevel where Period='4')
and Period='3';

select count(distinct csn), avg(Transactions/1.0), avg(LinkPoints), avg(UniqueMerchants/1.0)
from #customerlevel
where csn in (select csn from #customerlevel where Period='4')
and csn not in (select csn from #customerlevel where Period='3')
and csn not in (select csn from #customerlevel where Period='2')
and Period='1';

-- 150 days
select count(distinct csn), avg(Transactions/1.0), avg(LinkPoints), avg(UniqueMerchants/1.0)
from #customerlevel
where csn in (select csn from #customerlevel where Period='1')
and csn in (select csn from #customerlevel where Period='2')
and csn in (select csn from #customerlevel where Period='3')
and csn in (select csn from #customerlevel where Period='5')
and Period='4'
union
